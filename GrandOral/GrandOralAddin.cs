﻿using System;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using Microsoft.Office.Interop.PowerPoint;
using WebStatsScreen.Stats;

namespace WebStatsScreen
{
    public partial class GrandOralAddin
    {            
        public List<ScrapCounter> Counters { get; set; } = new List<ScrapCounter>();

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application.SlideShowBegin += OnBeginSlide;
            this.Application.SlideShowEnd += OnSlideShowEnd;
            //this.Application.SlideShowNextSlide += OnNextSlide;

        }

        private void OnSlideShowEnd(Presentation Pres)
        {
            DisposeAllActiveTimers();
        }

        private void DisposeAllActiveTimers()
        {
            Counters.ForEach(e =>
            {
                e.Dispose();
            });
            Counters.Clear();
        }

        private void OnBeginSlide(SlideShowWindow Wn)
        {
            foreach (Slide slide in this.Application.ActivePresentation.Slides)
            {
                foreach (Shape shape in slide.Shapes)
                {                   
                     AddShapeCounter(shape);                                   
                }
            }
            Counters.ForEach(e => e.Start());
        }
     
        public void AddShapeCounter(Shape shape)
        {
            ScrapCounter counter = Counters.FirstOrDefault(e => e.Name == shape.Tags[Constants.StatCounterName]);
            if (counter != null)
            {
                counter.Shapes.Add(shape);
            }
            else
            {
                Constants.Config.ForEach(p =>
                {
                    if (shape.Tags[Constants.StatCounterName] == p.Name)
                    {
                        ScrapCounter statsDataCounter = new ScrapCounter(p);
                        statsDataCounter.Shapes.Add(shape);
                        Counters.Add(statsDataCounter);
                    }
                });
            }
        }
       
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }


        #region Code généré par VSTO

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
