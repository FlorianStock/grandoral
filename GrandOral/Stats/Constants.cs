﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebStatsScreen.Stats
{
    public static class Constants
    {
        public const string StatCounterName = "WebStats";
        public const string StatCounterMode = "WebStatsConnected";

        public readonly static List<DataCounterConfig> Config = new List<DataCounterConfig>()
        {
            new DataCounterConfig()
            {
               Description = "Données publiées dans le monde en gigaOctets",
               Name = "GigaStat",
               DivId = "afficherStat1523",
               DivIdConnected = "Connecte1523",
               Url = "https://www.planetoscope.com/Internet-/1523-informations-publiees-dans-le-monde-sur-le-net-en-gigaoctets-.html",
            },
            new DataCounterConfig()
            {
               Description = "Cyber-attaques dans le monde",
               Name = "CyberAttacks",
               DivId = "afficherStat1852",
               DivIdConnected = "Connecte1852",
               Url = "https://www.planetoscope.com/Internet-/1852-cyberattaques-dans-le-monde.html",
            },
             new DataCounterConfig()
            {
               Description = "Spams envoyés dans le monde",
               Name = "CyberAttacksSpam",
               DivId = "afficherStat930",
               DivIdConnected = "Connecte930",
               Url = "https://www.planetoscope.com/Internet-/930-nombre-de-spams-envoyes-dans-le-monde.html",
            },
             new DataCounterConfig()
            {
               Description = "Emails envoyés dans le monde",
               Name = "CyberAttacksSpam",
               DivId = "afficherStat1024",
               DivIdConnected = "Connecte1024",
               Url = "https://www.planetoscope.com/Internet-/930-nombre-de-spams-envoyes-dans-le-monde.html",
            },
              new DataCounterConfig()
            {
               Description = "Personnes qui entrent sur le marché du travail",
               Name = "Workers",
               DivId = "afficherStat710",
               DivIdConnected = "Connecte710",
               Url = "https://www.planetoscope.com/demographie-urbanisme/710-nombre-de-personnes-qui-entrent-sur-le-marche-du-travail-dans-les-villes-de-chine.html",
            },
               new DataCounterConfig()
            {
               Description = "Emisisons de CO2 par habitant en France",
               Name = "C02",
               DivId = "afficherStat140",
               DivIdConnected = "Connecte140",
               Url = "https://www.planetoscope.com/co2/140-emissions-de-co2-par-habitant-en-france.html",
            },
        };
    }
}
