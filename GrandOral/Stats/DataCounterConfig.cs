﻿using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebStatsScreen.Stats
{
    public class DataCounterConfig
    {
        public string Url { get; set; }
        public string DivId { get; set; }
        public string DivIdConnected { get; set; }
        public string Description { get; set; } = "";
        public string Name { get; set; } = "";
   
    
    }
}
