﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace WebStatsScreen
{
    internal class ScrappingWebSite
    {
        private string _divId;
        private string _divConnected;
        private ChromeDriver _driver;

        public ScrappingWebSite(string url, string div, string divConnected)
        {
            _divId = div;
            _divConnected = divConnected;
         
            ChromeDriverService chromeService = ChromeDriverService.CreateDefaultService();
            chromeService.HideCommandPromptWindow = true;
            
            var options = new ChromeOptions();
            options.AddArgument("--headless");
            options.AddArgument("--user-agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36'");

            _driver = new ChromeDriver(chromeService,options);
            _driver.Navigate().GoToUrl(url);
           
        }

        public void Stop()
        {
            _driver.Close();
            _driver.Quit();
        }

        public string GetValue()
        {          
            IWebElement elem = _driver.FindElement(By.Id(_divId));
            return elem.Text;           
        }
        public string GetValueSinceConnected()
        {
            IWebElement elem = _driver.FindElement(By.Id(_divConnected));
            return elem.Text;
        }
    }

}
