﻿using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Timers;


namespace WebStatsScreen.Stats
{
    public class ScrapCounter : IDisposable
    {
        public string Name { get; set; }
        public List<Shape> Shapes { get; set; } = new List<Shape>();

        private readonly Timer _timer;      
        private ScrappingWebSite _scrappingWebSite;

        public ScrapCounter(DataCounterConfig config) 
        {
            Name = config.Name;

            _timer = new Timer(TimeSpan.FromMilliseconds(500).TotalMilliseconds);
            _timer.Elapsed += TimerTick;
            _scrappingWebSite = new ScrappingWebSite(config.Url, config.DivId, config.DivIdConnected);                  
        }    

        public void Start()
        {
            _timer.Start();
        }

        private void TimerTick(object _, ElapsedEventArgs __)
        {
            Shapes.ForEach(e =>
            {
                try
                {
                    string tagV = e.Tags[Constants.StatCounterMode].ToString();
                    string output = (tagV == "Connected") ? _scrappingWebSite.GetValueSinceConnected() : _scrappingWebSite.GetValue();

                    if (_timer.Enabled)
                    {
                        e.TextFrame.TextRange.Text = output;
                    }
                }
                catch(Exception ex)
                {

                }
               

            });                                                
        }

        public void Dispose()
        {
            Shapes.ForEach(e =>
            {
                e.TextFrame.TextRange.Text = "[Counter]";            
            });
            _timer.Dispose();
            _scrappingWebSite.Stop();
            
        }
    }
}
