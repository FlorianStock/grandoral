﻿using Microsoft.Office.Core;
using Microsoft.Office.Interop.PowerPoint;
using Microsoft.Office.Tools.Ribbon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebStatsScreen.Stats;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Menu;

namespace WebStatsScreen
{
    public partial class MainRibbon
    {
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {
            Constants.Config.ForEach(i =>
            {
                RibbonDropDownItem GigaInfoConnected = this.Factory.CreateRibbonDropDownItem();
                GigaInfoConnected.Label = i.Description;
                GigaInfoConnected.Tag = i.Name;
                dropDown1.Items.Add(GigaInfoConnected);
            });
           
        }

        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            var slide = FindActiveSlide();
            if (slide != null)
            {
                AddCounter(slide);
            }
        }
        private Slide FindActiveSlide()
        {
            var app = Globals.GrandOral_Addin.Application;
            if (app.ActivePresentation.Slides.Count > 0)
            {
                return app.ActiveWindow.View.Slide;
            }
            else
            {
                return null;
            }
        }

        private void AddCounter(Slide slide)
        {
            // Center the textBox on the Slide
            // 1. Calculate the top left corner 
            const float textBoxWidth = 180;
            const float textBoxHeight = 25;
            Presentation currentPresentation = Globals.GrandOral_Addin.Application.ActivePresentation;
            float slideHeight = currentPresentation.PageSetup.SlideHeight;
            float slideWidth = currentPresentation.PageSetup.SlideWidth;
            float x = (slideWidth - textBoxWidth) * 0.5f;
            float y = (slideHeight - textBoxHeight) * 0.5f;
            // 2. Place the slide
            var textBox = slide.Shapes.AddTextbox(
                MsoTextOrientation.msoTextOrientationHorizontal,
                x, y, textBoxWidth, textBoxHeight);

            textBox.TextFrame.TextRange.Text = "[Counter]";
            textBox.TextFrame.TextRange.Font.Size = 64;

            RibbonDropDownItem sel =  dropDown1.SelectedItem;
            textBox.Tags.Add(Constants.StatCounterName, sel.Tag.ToString());

            if(this.IsConnected.Checked == true)
            {
                textBox.Tags.Add(Constants.StatCounterMode, "Connected");
            }           
        }

        private void dropDown1_SelectionChanged(object sender, RibbonControlEventArgs e)
        {
           
        }

        private void IsConnected_Click(object sender, RibbonControlEventArgs e)
        {

        }
    }
}
